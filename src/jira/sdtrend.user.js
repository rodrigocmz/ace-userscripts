// ==UserScript==
// @name        SD Trend
// @namespace   Jira
// @match       *://*/browse/*
// @grant       GM_xmlhttpRequest
// @downloadURL https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/sdtrend.user.js
// @updateURL   https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/sdtrend.user.js
// @version     1.0.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez/)
// @description Example to alert of possible trends in similar Customer Request Types.
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// ==/UserScript==

const sdtrendThreshold = 3; // How many unresolved similar tickets should trigger the panel

/**
 * Injects the panel and links the search to the URL provided.
 * @param numberOfOtherCases number of similar cases returned.
 * @param linkURL the URL that the suer shuold be directed if clicking on "show me the cases".
 */
function injectSDTrendPanel(numberOfOtherCases, linkURL) {
  if (numberOfOtherCases >= sdtrendThreshold) {
    let sdtrendSidebar = document.getElementById("viewissuesidebar");
    let node = document.createElement("div");
    node.className = 'module toggle-wrap';
    node.id = "sdtrendmodule";
    node.innerHTML = '<div id="sdtrendmodule" class="module toggle-wrap"><div id="sdtrend_heading" class="mod-header"><ul class="ops"></ul><button class="aui-button toggle-title" aria-label="SD	Trend" aria-controls="sdtrendmodule" aria-expanded="true" resolved=""><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"><g fill="none" fill-rule="evenodd"><path d="M3.29175 4.793c-.389.392-.389 1.027 0 1.419l2.939 2.965c.218.215.5.322.779.322s.556-.107.769-.322l2.93-2.955c.388-.392.388-1.027 0-1.419-.389-.392-1.018-.392-1.406 0l-2.298 2.317-2.307-2.327c-.194-.195-.449-.293-.703-.293-.255 0-.51.098-.703.293z" fill="#344563"></path></g></svg></button><h3 class="toggle-title">🚨 There are <b>' + (numberOfOtherCases - 1) + ' other unresolved</b> similar tickets!</h3></div><div class="mod-content"><div class="item-details"><a href=\'' + linkURL + '\' target="_blank">Take me to them!</a></div></div></div>';
    sdtrendSidebar.insertBefore(node, sdtrendSidebar.firstChild);
  }
}

/**
 * Fetches the Request Type and searches for similar unresolved tickets.
 * If matching the threshold, shows the SD Trend panel.
 */
function runSDTrend() {
  let issueKey = window.location.href.split(/[/ ]+/).pop();
  GM_xmlhttpRequest({
    method:'GET',
    headers: {"Content-Type": "application/json"},
    url: window.location.href.split(/browse/).shift() + 'rest/servicedeskapi/request/' + issueKey + '?expand=requestType',
    responseType: 'json',
    onload: function(responseSD) {
      let requestType = responseSD.response.requestType.name;
      let jql = 'resolution = Unresolved AND "Customer Request Type" = "' + requestType + '"';
      GM_xmlhttpRequest({
        method:'GET',
        headers: {"Content-Type": "application/json"},
        url: window.location.href.split(/browse/).shift() + 'rest/api/2/search?jql=' + jql,
        responseType: 'json',
        onload: function(responseJQL) {
          let total = responseJQL.response.total;
          let linkURL = window.location.href.split(/browse/).shift() + 'issues/?jql=' + jql;
          injectSDTrendPanel(total, linkURL);
        }
      });
    }
  });
}

/*
 * MAIN BODY:
 */
setTimeout(function() {
  let slaPanel = document.getElementById("sla-web-panel");
  if (slaPanel) {
    runSDTrend();
  }
}, 1000); // Wait 1sec just in case :)