// ==UserScript==
// @name        Jira Currency
// @namespace   Jira
// @match       *://*/browse/*
// @grant       GM_xmlhttpRequest
// @downloadURL https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/jira-currency.user.js
// @updateURL   https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/jira-currency.user.js
// @version     1.0.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez/)
// @description ACEJC: Atlassian Community Events - Jira Currency example
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// ==/UserScript==

// HTML to be injected into Jira's "viewissue" screen:
const acejcDiv = '' +
    '<div class="wrap">' +
    '  <strong class="name">💵 Cotação do Dólar:</strong>' +
    '  <span id="ace-jc-span" class="value">' +
    '    <span class="note" id="ace-jc-value"></span>' +
    '  </span>' +
    '</div>';

/*
 * Returns a String representing today in format mm-dd-yyyy.
 */
function acejcGetDate(daysAgo = 0) {
    let d = new Date((new Date().getTime() - (daysAgo * 86400000)));
    return '' + String(d.getMonth() + 1).padStart(2, '0') + '-' + String(d.getDate()).padStart(2, '0') + '-' + d.getFullYear();
}

/*
 * Fetches today's Exchange.
 * @see https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/aplicacao#!/
 */
function acejcUpdateExchange() {
    let campo = document.getElementById('ace-jc-value');
    if (campo) {
        GM_xmlhttpRequest({
            method:'GET',
            headers: {"Content-Type": "application/json"},
            url: "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarPeriodo(dataInicial=@dataInicial,dataFinalCotacao=@dataFinalCotacao)?@dataInicial='" + acejcGetDate(5) + "'&@dataFinalCotacao='" + acejcGetDate(0) + "'&$top=1&$orderby=dataHoraCotacao%20desc&$format=json&$select=cotacaoCompra,cotacaoVenda,dataHoraCotacao",
            responseType: 'json',
            onload: function(responseBC) {
              campo.innerHTML = 'Compra: ' + responseBC.response.value[0].cotacaoCompra + ' / Venda: ' + responseBC.response.value[0].cotacaoVenda;
              campo.title = 'Cotação atualizada em: ' + responseBC.response.value[0].dataHoraCotacao;
            }
        });
    }
}

/* MAIN */
let issuedetails = document.getElementById('issuedetails');
if (issuedetails) {
  let newElem = document.createElement('li');
  newElem.innerHTML = acejcDiv;
  issuedetails.appendChild(newElem).className = "item full-width";
  acejcUpdateExchange();
}
