// ==UserScript==
// @name        Admin Check All
// @namespace   ACE UserScripts Examples
// @match       *://*/secure/admin/*
// @grant       none
// @downloadURL https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/admin-check-all.user.js
// @updateURL   https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/admin-check-all.user.js
// @version     1.0.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez/)
// @description Detects checkboxes on Admin screens and injects a button to check/uncheck them all.
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// ==/UserScript==

// Control Flag to check/uncheck the row of fields
let acaControlFlag = false;

/**
 * Checks/unchecks all boxes in the Custom Field Screen association screen.
 */
function adminCheckAllFieldScreens() {
  let boxes = document.getElementsByName('associatedScreens');
  if (boxes) {
    for (let i = 0; i < boxes.length; i++) {
      //console.info(boxes[i].checked)
      if (acaControlFlag)
        boxes[i].checked = ''
      else
        boxes[i].checked = 'checked';
    }
    acaControlFlag = !acaControlFlag;
  }
}

/*
 * Checks/unchecks the checboxes in the current screen.
 */
function adminCheckAll() {
  adminCheckAllFieldScreens();
  // Add more functions that work for other screens/checkboxes
}



/*
 * "MAIN" BODY:
 * Injects a button into the header based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 */
let boxes = document.getElementsByName('associatedScreens');
if (boxes) {
  let acaCreateButton = document.getElementById('create_link');
  if(acaCreateButton){
    let acaCreateButtonParent = acaCreateButton.parentNode.parentNode;
    var acaNode = document.createElement("li");
    acaNode.innerHTML = '<li class="aui-button aui-button-primary aui-style"><div id="admin-check-all">Check All</div></li>';
    acaCreateButtonParent.appendChild(acaNode);
    let adminCheckAllButton = document.getElementById("admin-check-all");
    adminCheckAllButton.addEventListener("click", adminCheckAll);
  }
}
