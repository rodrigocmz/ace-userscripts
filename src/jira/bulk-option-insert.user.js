// ==UserScript==
// @name        Bulk Option Insert (u)
// @namespace   ACE UserScripts Examples
// @match       *://*/secure/admin/ViewCustomFields*
// @grant       none
// @version     1.0.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez/)
// @description Userscript suggested by Daiane Conte, Atlassian Community Leader in São Paulo, Brazil. (https://www.linkedin.com/in/daianeconte/)
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// ==/UserScript==

// Change this list to the desired values!
const listBulkOptionInsert = ['Option A', 'Option B', 'Option C', 'Option D', 'Option E', 'Option F', '...'];

/*
 * Only works when creating a select list. Doesn't work (yet) when editing a field to include more options.
 */

// Access Key that triggers the button click because the button's hidden by the blurred background when creating custom fields
const boiAccessKey = 'u'; // "u" for userscript, maybe? ¯\_(ツ)_/¯

/**
 * Fills in the "custom-field-options-input" field and clicks the "custom-field-options-add" button for each value in [listBulkOptionInsert].
 */
function runBulkOptionInsert() {
    if (document.getElementById('custom-field-options-input')) {
        for (let i = 0; i < listBulkOptionInsert.length; i++) {
            document.getElementById('custom-field-options-input').value = listBulkOptionInsert[i];
            document.getElementById('custom-field-options-add').click();
        }
    }
}

/*
 * Fetching and injecting Jira button based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 * Works for both Server/DC and Cloud! :)
 */
let boiCreateButtonCloud = document.getElementById('createGlobalItem');
let boiCreateButtonDC = document.getElementById('create_link');

// For Cloud
if(boiCreateButtonCloud){
    let boiCreateButtonParent = boiCreateButtonCloud.parentNode.parentNode;
    let node = document.createElement("div");
    node.className = 'css-17l6d9b';
    node.innerHTML = '<button id="bulk-option-insert" data-hide-on-smallscreens="true" class="css-1tl77t8" type="button" tabindex="0" accesskey="' + boiAccessKey + '"><span class="css-19r5em7">Bulk Option Insert</span></button>';
    boiCreateButtonParent.appendChild(node);
    let boiButton = document.getElementById("bulk-option-insert");
    boiButton.addEventListener("click", runBulkOptionInsert);
}
else if (boiCreateButtonDC) { // For Server/DC
    let createButtonParent = boiCreateButtonDC.parentNode.parentNode;
    let node = document.createElement("li");
    node.innerHTML = '<li class="aui-button aui-button-primary aui-style"><div id="bulk-option-insert" accesskey="' + boiAccessKey + '">Bulk Option Insert</div></li>'
    createButtonParent.appendChild(node);
    let boiButton = document.getElementById("bulk-option-insert");
    boiButton.addEventListener("click", runBulkOptionInsert);
}
