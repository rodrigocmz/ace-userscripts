// ==UserScript==
// @name        Agile Boards Tooltips
// @namespace   ACE UserScripts Examples
// @match       *://*.atlassian.net/*boards*
// @grant       none
// @downloadURL https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/agile-boards-tooltips.user.js
// @updateURL   https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/agile-boards-tooltips.user.js
// @version     1.0.1
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez/)
// @description Agile Boards Tooltips
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// ==/UserScript==

const pageLoadDelayInMillis = 3000;

function injectTooltipForBoard1() {
  let boardColumn = document.querySelector("#ghx-column-headers > li:nth-child(1) > div > div");
  boardColumn.innerHTML += '<span class="ghx-iconfont aui-icon aui-icon-small aui-iconfont-help" title="Initial status!!!!\n\nThe\n team\n has\n discussed this issue but is yet to start \n\n\n\n\n\nworking on it."></span>';
  boardColumn = document.querySelector("#ghx-column-headers > li:nth-child(2) > div > div");
  boardColumn.innerHTML += '<span class="ghx-iconfont aui-icon aui-icon-small aui-iconfont-help" title="The team is already working on this issue."></span>';
  boardColumn = document.querySelector("#ghx-column-headers > li:nth-child(3) > div > div");
  boardColumn.innerHTML += '<span class="ghx-iconfont aui-icon aui-icon-small aui-iconfont-help" title="Final status.\n\nThe team has already finished work on this issue and is waiting for the end of the Sprint to publish it."></span>';
}

// Delay for the page to load the necessary elements
setTimeout(function() {
  let header = document.getElementById('ghx-column-headers');
  if (header) {
    const boardId = window.location.href.split(/boards\//).pop().split(/\?/).shift();
    switch (boardId) {
      case '1':
        injectTooltipForBoard1();
        break;
        // More cases for each known Board Id.
      }
  }
}, pageLoadDelayInMillis);
