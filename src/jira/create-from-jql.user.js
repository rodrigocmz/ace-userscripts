// ==UserScript==
// @name        Create From JQL
// @namespace   ACE UserScripts Examples
// @match       *://*/issues/*
// @grant       GM_xmlhttpRequest
// @downloadURL https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/create-from-jql.user.js
// @updateURL   https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/create-from-jql.user.js
// @version     1.0.0
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez/)
// @description Atlassian Community Events - Create From JQL example (KCS theme).
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// ==/UserScript==

const KCSProjectId = 10003
const KCSIssueTypeId = 10010
const KCSKnowledgeBaseLink = 'customfield_10135';
const KCSJQLPrefix = 'resolution != Unresolved AND labels != kcs-par AND '
const KCSLabel = 'kcs-par'

const KCSVersion = '1.0.0'

/**
 * Injects a spinner "kcs-spinner" into the bottom of "kcs-dynamic-content" div.
 */
function injectKCSSpinner() {
    // Spinner from https://aui.atlassian.com/aui/7.9/docs/spinner.html
    let dialog = document.getElementById('kcs-dynamic-content')
    let block = '<div id="kcs-spinner"><center><aui-spinner size="medium"></aui-spinner></center></div>'
    dialog.innerHTML += block
}

/**
 * Removes the first occurrence of spinner "kcs-spinner" wherever it is in the page.
 */
function removeKCSSpinner() {
    let spinner = document.getElementById('kcs-spinner')
    if (spinner != undefined && spinner != null) {
        spinner.remove()
    }
}

/**
 * Injects the "kcs-commit-button" commit button into the bottom of "kcs-dynamic-content" div.
 */
function injectKCSCommitButton() {
    let dialog = document.getElementById('kcs-dynamic-content')
    let block = '<div class="aui-button aui-button-primary" id="kcs-commit-button">Commit</div>'
    dialog.innerHTML += block + '<p/>'
    let newButton = document.getElementById('kcs-commit-button')
    newButton.addEventListener("click", commitKCS);
}

/**
 * Removes the first occurrence of "kcs-commit-button" button wherever it is in the page.
 * This is useful to prevent committing two times in a row.
 */
function removeKCSCommitButton() {
    let button = document.getElementById('kcs-commit-button')
    if (button) button.remove()
}

/**
 * Clears the content of the JQL results.
 * Currently not in use.
 */
function clearKCSResults() {
    let content = document.getElementById('kcs-dynamic-content')
    content.innerHTML = '<p/>'
}

/**
 * Returns a string containing the JSON representaion of the whole data content for Jira's "POST /rest/issue/bulk".
 * @param issueJSON the string/JSON representation of the "issues" attribute from the return of Jira's "/rest/search".
 */
function buildIssuesForKCSCreation(issuesJSON) {
    let content = '{"issueUpdates":['
    for (let i = 0; i < issuesJSON.length; i++) {
        if (i == 0) content += buildIssueForKCSCreation(issuesJSON[i])
        else content += ', ' + buildIssueForKCSCreation(issuesJSON[i])
    }
    content += ']}'
    return content;
}

/**
 * Returns a string with the JSON representation of each issue for Jira's "POST /rest/issue/bulk".
 * @see buildIssuesForCreation(issuesJSON)
 * @param issueJSON the string/JSON representation of a single issue fetched from Jira's "/rest/search".
 */
function buildIssueForKCSCreation(issueJSON) {
    let content = '{"update":{},"fields":{'
    content += '"project":{"id":"' + KCSProjectId + '"}'
    content += ', "issuetype":{"id":"' + KCSIssueTypeId + '"}'
    content += ', "summary":"PAR for ' + issueJSON.key + '"'
    content += ', "description":"[How to Review Process Adherence - PAR|https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/040/020/060]"'
    if (eval('issueJSON.fields.' + KCSKnowledgeBaseLink))
        content += ', "' + KCSKnowledgeBaseLink + '":"' + eval('issueJSON.fields.' + KCSKnowledgeBaseLink) + '"'
    if (issueJSON.fields.assignee) {
        content += ', "reporter":{"name": "' + issueJSON.fields.assignee.name + '"}'
    }
    content += '}}'
    return content;
}

/**
 * Adds the label kcs- to the issue.
 * Makes use of window.KCS.KCSJQLResults populated previously.
 */
function kcsLabelIssues() {
    if (window.KCS.KCSJQLResults) {
        for (let i = 0; i < window.KCS.KCSJQLResults.length; i++) {
            GM_xmlhttpRequest({
                method:'PUT',
                headers: {"Content-Type": "application/json"},
                url: window.location.href.split(/issues/).shift() + 'rest/api/2/issue/' + window.KCS.KCSJQLResults[i].id,
                responseType: 'json',
                data: '{"update": {"labels":[{"add":"'+ KCSLabel + '"}]}}',
                onload: function(data) {
                    console.info('Labeled issue: ' + window.KCS.KCSJQLResults[i].key)
                    window.KCS.labeledIssueCount++;
                }
            });
        }
    }
}

/**
 * Injects the light box for KCS PAR.
 * This is triggered by the "kcs-button", which is injected when the script runs on page load.
 * @see this
 */
function injectKCSLightBox() {
    const newDiv = document.createElement("div");
    newDiv.innerHTML = '' +
        '<div id="kcs-dialog" class="jira-dialog box-shadow jira-dialog-open popup-width-custom jira-dialog-content-ready" style="width: 810px; margin-left: -406px; margin-top: -281px;">' +
        '  <div class="jira-dialog-heading">' +
        '    <div class="aui-toolbar2 qf-form-operations" role="toolbar">' +
        '      <div class="aui-toolbar2-inner">' +
        '        <div class="aui-toolbar2-secondary">' +
        '          <a href="#" id="kcs-close-button" accesskey="`" title="Press Ctrl+Alt+` to close" class="cancel">Close</a>' +
        '        </div>' +
        '      </div>' +
        '    </div>' +
        '    <h2 title="KB Review">KCS PAR Creation</h2>' +
        '  </div>' +
        '  <div class="jira-dialog-content" id="kcs-dialog-content">' +
        '    <div class="qf-container">' +
        '      <div class="qf-unconfigurable-form">' +
        '        <form name="jiraform" action="#" class="aui">' +
        '          <div class="form-body" style="max-height: 413px;">' +
        '            <div class="issue-setup-fields">' +
        '              <div class="qf-field qf-field-project qf-required" data-field-id="project">' +
        '                <div class="twixi-wrap verbose actionContainer">' +
        '                  <div class="action-head">' +
        '                    <div class="sd-comment-action-details action-details">' +
        '                      <div class="sd-action-details-verbose">' +
        '                        <div class="sd-comment-primary-details">' +
        '                          <div class="action-body flooded">' +
        '                            <h3>KCS PAR Creation helper</h3>' +
        '                            <div class="panel" style="background-color: #eae6ff;border-width: 1px;">' +
        '                              <div class="panelContent" style="background-color: #eae6ff;">' +
        '                               <input class="text long-field" id="kcs-jql" name="kcs-jql" type="text" placeholder="Type the JQL here">' +
        '                               <p>Provide the JQL above to filter the tickets that you want PAR issues to be created from.</p>' +
        '                               <p>This gadget will only fetch & create 50 at a time, so you may execute it multiple times.<br/>' +
        '                               <sub><em>(Refer to <b><a href="https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/040/020/060" class="external-link" target="_blank" rel="nofollow noopener">How to create PAR issues</a></b>)</em></sub></p>' +
        '                               <p><div class="aui-button aui-button-primary" id="kcs-jql-button">Run JQL</div></p>' +
        '                              </div>' +
        '                            </div>' +
        '                          </div>' +
        '                        </div>' +
        '                      </div>' +
        '                    </div>' +
        '                  </div>' +
        '                </div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="content" id="kcs-dynamic-content"><p/></div>' +
        '          </div>' +
        '          <div class="buttons-container form-footer">' +
        '            <p><sub><em><a href="https://bitbucket.org/rodrigocmz/ace/src/master/src/jira/" class="external-link" target="_blank" rel="nofollow noopener">KCS PAR Creation Helper</a> (version ' + KCSVersion + ')</em></sub></p>' +
        '          </div>' +
        '        </form>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>'
    document.body.appendChild(newDiv);
    let closeBoxButton = document.getElementById('kcs-close-button');
    closeBoxButton.addEventListener("click", closeKCSLightBox);
    let kcsPARJQLButton = document.getElementById("kcs-jql-button");
    kcsPARJQLButton.addEventListener("click", runKCS);
}

/**
 * Closes the KCS PAR Helper light box.
 */
function closeKCSLightBox() {
    let kcspardialog = document.getElementById('kcs-dialog');
    if(!kcspardialog) {
        return;
    }
    kcspardialog.parentNode.removeChild(kcspardialog);
}

/**
 * Executes the JQL search and prints and stores the results in the array window.KCS.KCSJQLResults.
 */
function runKCS() {
    injectKCSSpinner()
    jql = document.getElementById("kcs-jql").value;
    if (!jql) {
        jql = "created > now()"; // Mechanism to prevent fetch all
    }
    else {
        jql = KCSJQLPrefix + jql;
    }
    console.error(window.location.href.split(/issues/).shift() + 'rest/api/2/search?jql=' + jql)
    GM_xmlhttpRequest({
        method:'GET',
        url: window.location.href.split(/issues/).shift() + 'rest/api/2/search?jql=' + jql,
        responseType: 'json',
        onload: function(dataGSAC) {
            clearKCSResults();
            window.KCS.KCSJQLResults = dataGSAC.response.issues;
            let results = document.getElementById("kcs-dynamic-content");
            for (let i = 0; i < window.KCS.KCSJQLResults.length; i++) {
                if (i == 0) results.innerHTML += window.KCS.KCSJQLResults[i].key;
                else results.innerHTML += ', ' + window.KCS.KCSJQLResults[i].key;
            }
            removeKCSSpinner();
            results.innerHTML += '<p/>';
            injectKCSCommitButton();
        }
    });
}

/**
 * Creates the KNOW issues for PAR. It reads them from the array window.KCS.KCSJQLResults, previously populated.
 * @see runKCS()
 */
function commitKCS() {
    removeKCSCommitButton();
    injectKCSSpinner();
    let data = buildIssuesForKCSCreation(window.KCS.KCSJQLResults);
    GM_xmlhttpRequest({
        method:'POST',
        headers: {"Content-Type": "application/json"},
        url: window.location.href.split(/issues/).shift() + 'rest/api/2/issue/bulk',
        responseType: 'json',
        data: data,
        onload: function(dataReturn) {
          console.info(dataReturn.response);
          if (dataReturn.status == 201) {
            kcsLabelIssues();
          }
          setTimeout(function(){
              removeKCSSpinner();
          }, 15000);
          let results = document.getElementById("kcs-dynamic-content");
          results.innerHTML += '[' + dataReturn.status + '] ' + dataReturn.response;
          results.innerHTML += '<p/>';
          results.innerHTML += '<p/>' + data;
        }
    });
}

// "MAIN":

window.KCS = {};
window.KCS.KCSJQLResults = [];
window.KCS.labeledIssueCount = 0;

/*
 * Injects a button into the header based on: https://codereview.stackexchange.com/questions/204768/tampermonkey-script-to-add-a-button-to-jira
 */
let kbrCreateButton = document.getElementById('create_link');
if(kbrCreateButton){
    let kbrCreateButtonParent = kbrCreateButton.parentNode.parentNode;
    var kbrNode = document.createElement("li");
    kbrNode.innerHTML = '<li class="aui-button aui-button-primary aui-style"><div id="kcs-button">KCS PAR</div></li>';
    kbrCreateButtonParent.appendChild(kbrNode);
    let KCSButton = document.getElementById("kcs-button");
    KCSButton.addEventListener("click", injectKCSLightBox);
}
