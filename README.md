## Atlassian Community Events

This repo contains some examples of [userscripts](https://en.wikipedia.org/wiki/Userscript) to be used with a [userscript manager extension](https://en.wikipedia.org/wiki/Userscript_manager) in a browser.

* These scripts were meant as POCs (proof of concept) and learning material. They are not optimized or carefully thought for production use.
* They are also not meant as good examples of the Javascript language or HTML building.
 
---

## Installing the scripts

1. Install a userscript manager. [Violent Monkey](https://violentmonkey.github.io/)'s an interesting choice as it works for several browsers and is very streamlined.
2. Open the userscript manager dashboard and add scripts from URL.
3. Paste the URLs from the **raw** path, like: https://bitbucket.org/rodrigocmz/ace-userscripts/raw/master/src/jira/admin-check-all.user.js

---

## Userscripts brief intro
**Admin Check All**

Works on Jira Server/DC admin screens with lots of checkboxes. Currently only working for the *custom field screen association* screen.


**Jira Currency**

Works on Jira Server/DC *browse issue* screen. Prints the Dolar exchange in Brazil's currency.


**Agile Boards Tooltips**

Works on Jira Cloud agile boards. Currently only injects tooltips for *rapidView=1*.


**Create From JQL**

An example on creating issues off a JQL result. Requires some edition to variables to work anywhere else. It's just a POC.

---

This work by [Rodrigo Martinez](https://www.linkedin.com/in/rodrigo-carvalho-martinez/) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

~ [Rodrigo Martinez](https://www.linkedin.com/in/rodrigo-carvalho-martinez/)